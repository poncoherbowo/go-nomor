package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

// Nomor type
type Nomor struct {
	NoHP string `json:"nohp"`
}

// Operator type
type Operator struct {
	Telkomsel []string `json:"telkomsel"`
	XL        []string `json:"xl"`
	Indosat   []string `json:"indosat"`
	Tri       []string `json:"tri"`
	SmartFren []string `json:"smart"`
	Axis      []string `json:"axis"`
	Others    []string `json:"others"`
}

// Route type
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes type
type Routes []Route

// Noauth routes
var routes = Routes{
	Route{
		"pisah",
		"POST",
		"/pisah",
		pisahNomor,
	},
	Route{
		"hello",
		"GET",
		"/",
		hello,
	},
}

func main() {
	router := Handlers()
	port := "3000"

	// serve
	log.Printf("Server up on port '%s'", port)
	log.Fatal(http.ListenAndServe(GetPort(), router))
}

// GetPort from the environment so we can run on Heroku
func GetPort() string {
	var port = os.Getenv("PORT")
	// Set a default port if there is nothing in the environment
	if port == "" {
		port = "4747"
		fmt.Println("INFO: No PORT environment variable detected, defaulting to " + port)
	}
	return ":" + port
}

func hello(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode("Hello")
}

func pisahNomor(w http.ResponseWriter, r *http.Request) {
	var operator Operator
	n := r.FormValue("nohp")
	no := strings.Split(n, "\n")

	reg, _ := regexp.Compile("[^0-9]+")

	var telkomsel = []string{"0811", "0812", "0813", "0821", "0822", "0823", "0852", "0853", "0851"}
	var xl = []string{"0817", "0818", "0819", "0859", "0877", "0878"}
	var indosat = []string{"0814", "0815", "0816", "0855", "0856", "0857", "0858"}
	var tri = []string{"0895", "0896", "0897", "0898", "0899"}
	var smart = []string{"0881", "0882", "0883", "0884", "0885", "0886", "0887", "0888", "0889"}
	var axis = []string{"0838", "0831", "0832", "0833"}

	for _, val := range no {
		runeStr := []rune(reg.ReplaceAllString(val, ""))
		prefix := string(runeStr[0:4])

		ifTelkomsel, _ := inArray(prefix, telkomsel)
		ifXL, _ := inArray(prefix, xl)
		ifIndosat, _ := inArray(prefix, indosat)
		ifTri, _ := inArray(prefix, tri)
		ifSmart, _ := inArray(prefix, smart)
		ifAxis, _ := inArray(prefix, axis)

		reg := reg.ReplaceAllString(val, "")
		if len(reg) <= 14 {
			if ifTelkomsel != false {
				operator.Telkomsel = append(operator.Telkomsel, reg)
			} else if ifXL != false {
				operator.XL = append(operator.XL, reg)
			} else if ifIndosat != false {
				operator.Indosat = append(operator.Indosat, reg)
			} else if ifTri != false {
				operator.Tri = append(operator.Tri, reg)
			} else if ifSmart != false {
				operator.SmartFren = append(operator.SmartFren, reg)
			} else if ifAxis != false {
				operator.Axis = append(operator.Axis, reg)
			} else {
				operator.Others = append(operator.Others, reg)
			}
		}

	}
	json.NewEncoder(w).Encode(operator)
}

// inArray function
func inArray(val interface{}, array interface{}) (exists bool, index int) {
	exists = false
	index = -1

	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)
		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) == true {
				index = i
				exists = true
				return
			}
		}
	}

	return
}

// Handlers handle all endpoint
func Handlers() *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	r.Use(CommonMiddleware)

	for _, route := range routes {
		var handler http.Handler

		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		r.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return r
}

// CommonMiddleware --Set content-type
func CommonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Access-Control-Request-Headers, Access-Control-Request-Method, Connection, Host, Origin, User-Agent, Referer, Cache-Control, X-header")
		next.ServeHTTP(w, r)
	})
}

// Logger for showing log
func Logger(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		inner.ServeHTTP(w, r)

		log.Printf(
			"%s\t%s\t%s\t%s",
			r.Method,
			r.RequestURI,
			name,
			time.Since(start),
		)
	})
}
